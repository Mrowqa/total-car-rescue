#!/usr/bin/python

import i2c
from threading import Lock


class MCP3424:
    """
    ADC (analog to digital converter) python helper class. This module
    requires driver for MCP3424 module which is available in Linux kernel tree
    but is disabled by default, so you should recompile it.
    Note: class requires already created device in /sys/bus/i2c/devices/
    """

    AVAILABLE_PROBING_FREQUENCY_IN_HERTZ = [240, 60, 15, 3]  # should be 3.75 Hz instead of 3, but driver operates on 3
    AVAILABLE_PROBING_RESOLUTIONS_IN_BITS = [12, 14, 16, 18]
    AVAILABLE_PROBING_MODES = [{'frequency': AVAILABLE_PROBING_FREQUENCY_IN_HERTZ[i],
                                'resolution': AVAILABLE_PROBING_RESOLUTIONS_IN_BITS[i]}
                               for i in range(len(AVAILABLE_PROBING_FREQUENCY_IN_HERTZ))]

    __PATH_PATTERN = R'/sys/bus/i2c/devices/i2c-{i2c_rev}/{i2c_rev}-00{i2c_address:02x}/iio:device0'
    __MODE_PATH_SUFFIX = '/in_voltage_sampling_frequency'
    __INPUT_RAW_READING_PATH_SUFFIX_PATTERN = '/in_voltage{id}_raw'

    __DEVICE_LOCK = Lock()

    def __init__(self, i2c_address=0x6A):
        self.__i2c_address = i2c_address
        self.__driver_path = self.__PATH_PATTERN.format(i2c_rev=i2c.raspberry_pi_bus_number(), i2c_address=i2c_address)

    def get_mode(self):
        """
        :return: a dictionary with 'frequency' and 'resolution' keys
        """
        self.__DEVICE_LOCK.acquire()
        with open(self.__driver_path + self.__MODE_PATH_SUFFIX, 'r') as f:
            frequency = int(f.read())
        self.__DEVICE_LOCK.release()
        mode_index = self.AVAILABLE_PROBING_FREQUENCY_IN_HERTZ.index(frequency)
        return self.AVAILABLE_PROBING_MODES[mode_index]

    def set_mode(self, mode):
        """
        Disclaimer: script must have writing access to device driver's configuration files
        """
        self.set_mode_by_frequency(mode[0])

    def set_mode_by_resolution(self, resolution):
        """
        Disclaimer: script must have writing access to device driver's configuration files
        """
        mode_index = self.AVAILABLE_PROBING_RESOLUTIONS_IN_BITS.index(resolution)
        frequency = self.AVAILABLE_PROBING_FREQUENCY_IN_HERTZ[mode_index]
        self.set_mode_by_frequency(frequency)

    def set_mode_by_frequency(self, frequency):
        """
        Disclaimer: script must have writing access to device driver's configuration files
        """
        self.__DEVICE_LOCK.acquire()
        with open(self.__driver_path + self.__MODE_PATH_SUFFIX, 'w') as f:
            f.write(str(frequency))
        self.__DEVICE_LOCK.release()

    def read_analog(self, pin_id):
        """
        Method reads analog value from ADC.
        :param pin_id: pin ID, could be one of the following: 0,1,2,3
        :return: returns normalized reading from given pin (value in range -1..1)
        """
        raw_reading_path = self.__driver_path + self.__INPUT_RAW_READING_PATH_SUFFIX_PATTERN.format(id=pin_id)
        self.__DEVICE_LOCK.acquire()
        with open(raw_reading_path, 'r') as f:
            raw_reading = int(f.read())
        self.__DEVICE_LOCK.release()
        resolution = self.get_mode()['resolution']  # Tip: think again about caching this value
        scale_factor = 1.0 / (2 ** (resolution - 1) - 1)
        return raw_reading * scale_factor
