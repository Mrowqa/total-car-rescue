#!/usr/bin/python

import i2c


class LSM303D:
    """
    Accelerometer and magnetometer sensors
    """
    BUS = i2c.get_bus()
    ADDRESS = 0x1D  # 0b0011101

    ACCELERATION_DATA_RATE = {3.125: 0b0001, 6.25: 0b0010, 12.5: 0b0011, 25: 0b0100, 50: 0b0101, 100: 0b0110,
                              200: 0b0111, 400: 0b1000, 800: 0b1001, 1600: 0b1010}
    # [kHz]
    ACCELERATION_ALL_AXES_ENABLED = 0b111
    ACCELERATION_FULL_SCALE_SELECTION = {'+-2 g': 0b000, '+-4 g': 0b001,
                                         '+- 6g': 0b010, '+-8 g': 0b011, '+-16 g': 0b100}
    ACCELERATION_LSB_TO_MG_FACTOR = {0b000: 0.061, 0b001: 0.122, 0b010: 0.183,
                                     0b011: 0.244, 0b100: 0.732}
    # calibrated at 2.5V

    MAGNETOMETER_MODE = {'power_down': 0b10, 'continuous_conversion': 0b00,
                         'single_conversion': 0b01}
    MAGNETOMETER_RESOLUTION = [0b00, 0b01, 0b10, 0b11]
    MAGNETOMETER_DATA_RATE = {3.125: 0b000, 6.25: 0b001, 12.5: 0b010, 25: 0b011,
                              50: 0b100, 100: 0b101}
    # NOTE: [100 kHz available when] accelerometer data rate >50 kHz or
    # accelerometer sensor disabled
    MAGNETOMETER_FULL_SCALE_SELECTION = {'+-2 gauss': 0b00, '+-4 gauss': 0b01,
                                         '+-8 gauss': 0b10, '+-12 gauss': 0b11}
    MAGNETOMETER_LSB_TO_MGAUSS_FACTOR = {0b00: 0.080, 0b01: 0.160, 0b10: 0.320,
                                         0b11: 0.479}
    # calibrated at 2.5V

    CTRL1_REGISTER = 0x20
    CTRL2_REGISTER = 0x21
    CTRL5_REGISTER = 0x24
    CTRL6_REGISTER = 0x25
    CTRL7_REGISTER = 0x26

    ACCELERATION_OUTPUT_XYZ = {'x': 0x28, 'y': 0x2A, 'z': 0x2C}
    MAGNETOMETER_OUTPUT_XYZ = {'x': 0x08, 'y': 0x0A, 'z': 0x0C}

    def __init__(self, acc_data_rate=12.5, mag_data_rate=12.5,
                 acc_scale_selection='+-2 g', mag_scale_selection='+-2 gauss'):

        settings = (self.ACCELERATION_DATA_RATE[acc_data_rate] << 4) | \
                    self.ACCELERATION_ALL_AXES_ENABLED
        i2c.write_byte(self.BUS, self.ADDRESS, self.CTRL1_REGISTER, settings)

        settings = (self.ACCELERATION_FULL_SCALE_SELECTION[acc_scale_selection] << 3)
        i2c.write_byte(self.BUS, self.ADDRESS, self.CTRL2_REGISTER, settings)

        settings = (self.MAGNETOMETER_RESOLUTION[-1] << 5) | \
                   (self.MAGNETOMETER_DATA_RATE[mag_data_rate] << 2)
        i2c.write_byte(self.BUS, self.ADDRESS, self.CTRL5_REGISTER, settings)

        settings = (self.MAGNETOMETER_FULL_SCALE_SELECTION[mag_scale_selection] << 5)
        i2c.write_byte(self.BUS, self.ADDRESS, self.CTRL6_REGISTER, settings)

        settings = (self.MAGNETOMETER_MODE['continuous_conversion'])
        i2c.write_byte(self.BUS, self.ADDRESS, self.CTRL7_REGISTER, settings)

    def get_acceleration(self):
        """
        Returns acceleration in mg units, where g is acceleration of Earth's
        gravity and m is a 'milli' prefix equal 10**(-3).
        """

        result_xyz = [0] * 3
        axis2index = dict(x=0, y=1, z=2)

        for axis, address in self.ACCELERATION_OUTPUT_XYZ.items():
            l_byte = i2c.read_byte(self.BUS, self.ADDRESS, address)
            h_byte = i2c.read_byte(self.BUS, self.ADDRESS, address + 1)

            value = (h_byte << 8) | l_byte
            value = i2c.twos_complement(value, 16)
            result_xyz[axis2index[axis]] = value

        afs = self.__get_acceleration_full_scale()
        unit_factor = self.ACCELERATION_LSB_TO_MG_FACTOR[afs]
        result_xyz = [i * unit_factor for i in result_xyz]
        return result_xyz

    def get_magnetic_data(self):  # TODO refactor: extract method for reading
        #  XYZ data for acceleration and magnetic data
        """
        Returns magnetic data in milligauss units.
        """

        result_xyz = [0] * 3
        axis2index = dict(x=0, y=1, z=2)

        for axis, address in self.MAGNETOMETER_OUTPUT_XYZ.items():
            l_byte = i2c.read_byte(self.BUS, self.ADDRESS, address)
            h_byte = i2c.read_byte(self.BUS, self.ADDRESS, address + 1)

            value = (h_byte << 8) | l_byte
            value = i2c.twos_complement(value, 16)
            result_xyz[axis2index[axis]] = value

        mfs = self.__get_magnetometer_full_scale()
        unit_factor = self.MAGNETOMETER_LSB_TO_MGAUSS_FACTOR[mfs]
        result_xyz = [i * unit_factor for i in result_xyz]
        return result_xyz

    def __get_acceleration_full_scale(self):
        settings = i2c.read_byte(self.BUS, self.ADDRESS, self.CTRL2_REGISTER)
        return (settings >> 3) & 0b111

    def __get_magnetometer_full_scale(self):
        settings = i2c.read_byte(self.BUS, self.ADDRESS, self.CTRL6_REGISTER)
        return (settings >> 5) & 0b11
