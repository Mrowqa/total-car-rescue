#!/usr/bin/python

from sensors import AD8232
from time import sleep
from sys import argv

pulsometer = AD8232(1, 0x6A)

WIDTH = 69
frequency = float(argv[1]) if len(argv) >= 2 else 100
while True:
    reading = pulsometer.get_current_ecg_value()
    space_count_before = int(reading * WIDTH)
    space_count_after = 70 - 1 - space_count_before
    print('{:.04f} |{}#{}|'.format(reading, ' '*space_count_before, ' '*space_count_after))
    sleep(1.0/frequency)
