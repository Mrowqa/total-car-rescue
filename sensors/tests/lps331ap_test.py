#!/usr/bin/python

from sensors import LPS331AP
from time import sleep

lps = LPS331AP()

while True:
    print('Pressure [mbar]: ' + str(lps.get_pressure()))
    print('Temperature [*C]: ' + str(lps.get_temperature()))
    print('')
    sleep(1)
