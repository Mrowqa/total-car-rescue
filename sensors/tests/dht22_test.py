#!/usr/bin/python

from sensors import DHT22
from time import sleep

dht = DHT22()

while True:
    print('Humidity [%]: ' + str(dht.get_humidity()))
    print('Temperature [*C]: ' + str(dht.get_temperature()))
    print('')
    sleep(1)
