#!/usr/bin/python

from sensors import L3GD20H
from time import sleep

gyroscope = L3GD20H()
gyroscope.set_calibration_offset(-1.75, +9, -1.5)

while True:
    print('XYZ [*/s]: ' + str(gyroscope.get_xyz_angular_velocity()))
    sleep(1)
