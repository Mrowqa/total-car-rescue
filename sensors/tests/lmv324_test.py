#!/usr/bin/python

from sensors import LMV324
from time import sleep

mic = LMV324(0, 0x6A)

while True:
    print('Sound volume [dB]: ' + str(mic.get_sound_volume()))  # TODO: convert raw data to dB
    sleep(1)
