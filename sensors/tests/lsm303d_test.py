#!/usr/bin/python

from sensors import LSM303D
from time import sleep

lsm = LSM303D()

while True:
    print('Acceleration [mg]:      ' + str(lsm.get_acceleration()))
    print('Magnetic data [mgauss]: ' + str(lsm.get_magnetic_data()))
    print('')
    sleep(1)
