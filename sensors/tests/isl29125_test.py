#!/usr/bin/python

from sensors import ISL29125
from time import sleep

isl = ISL29125()

while True:
    print('RGB [lx]: ' + str(isl.get_rgb_illuminance()))
    sleep(1)
