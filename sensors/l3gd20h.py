#!/usr/bin/python

import i2c


class L3GD20H:
    """
    Gyroscope sensor
    """
    BUS = i2c.get_bus()
    ADDRESS = 0x6B  # 0b1101011

    OPERATING_MODE = dict(power_down=0b0000, sleep=0b1000, x=0b1010, y=0b1001,
                          z=0b1100, xy=0b1011, xz=0b1110, yz=0b1101, xyz=0b1111)
    LSB_TO_DPS_WITH_DEFAULT_FS = 8.75 / 1000
    # FIXME - this factor does *NOT* seem correct. (or needs calibration)
    # Default full scale = 245 DPS

    CONFIGURATION_1_REGISTER = 0x20
    XYZ_DPS_REGISTERS = dict(x=0x28, y=0x2A, z=0x2C)

    def __init__(self, operating_mode='xyz'):
        self.__calibration_offset = (0, 0, 0)
        #  self.set_calibration_offset(0, 0, 0)
        settings = self.OPERATING_MODE[operating_mode]
        i2c.write_byte(self.BUS, self.ADDRESS, self.CONFIGURATION_1_REGISTER,
                       settings)

    def get_xyz_angular_velocity(self):
        """
        Returns list containing angular velocity in dps units (degrees per
        second) for each of axes (x, y, z).
        """

        result_xyz = [0] * 3
        axis2index = dict(x=0, y=1, z=2)

        for axis, address in self.XYZ_DPS_REGISTERS.items():
            l_byte = i2c.read_byte(self.BUS, self.ADDRESS, address)
            h_byte = i2c.read_byte(self.BUS, self.ADDRESS, address + 1)
            value = (h_byte << 8) | l_byte
            value = i2c.twos_complement(value, 2 * 8)
            result_xyz[axis2index[axis]] = value

        for i in range(3):
            result_xyz[i] *= self.LSB_TO_DPS_WITH_DEFAULT_FS
            result_xyz[i] += self.__calibration_offset[i]

        return result_xyz

    def set_calibration_offset(self, x, y, z):
        self.__calibration_offset = x, y, z
