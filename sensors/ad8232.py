#!/usr/bin/python

from mcp3424 import MCP3424


class AD8232:
    """
    Pulsometer (heart monitor)
    """

    def __init__(self, pulse_pin_id=1, adc_i2c_address=0x6A):
        self.__adc = MCP3424(adc_i2c_address)
        self.__pulse_pin_id = pulse_pin_id

    def get_current_ecg_value(self):
        """
        :return: normalized value (in range 0..1) of "EKG"
        """
        normalized_reading = self.__adc.read_analog(self.__pulse_pin_id)
        # FIXME: uncomment this code. It has been commented out due to some problems with ADC/pulsometer.
        # if normalized_reading < 0:
        #     raise IOError('Heart monitor is not connected! Check signal pin in ADC, should be connected to pin id={}'
        #                   .format(self.__pulse_pin_id))
        return normalized_reading
