#!/usr/bin/python

import i2c


class LPS331AP:
    """
    Pressure sensor
    """
    BUS = i2c.get_bus()
    ADDRESS = 0x5D  # 0b1011101

    OPERATING_MODE = dict(active=0b1, power_down=0b0)
    DATA_RATE = {(0, False): 0b000, (1, False): 0b001, (7, False): 0b010,
                 (12.5, False): 0b011, (25, False): 0b100, (7, True): 0b101,
                 (12.5, True): 0b110, (25, True): 0b111}
    # pressure output date rate, do probe temperature as fast like pressure?

    # TODO - add support for diff & delta pressure

    CONFIGURATION_1_REGISTER = 0x20
    PRESSURE_REGISTER = 0x28
    TEMPERATURE_REGISTER = 0x2B

    def __init__(self, operating_mode='active', data_rate=(12.5, True)):
        settings = (self.OPERATING_MODE[operating_mode] << 7) | \
                   (self.DATA_RATE[data_rate] << 4)
        i2c.write_byte(self.BUS, self.ADDRESS, self.CONFIGURATION_1_REGISTER,
                       settings)

    def get_pressure(self):
        """
        Returns pressure in millibar units.
        """

        bytes = [0] * 3
        for i in range(3):
            bytes[i] = i2c.read_byte(self.BUS, self.ADDRESS, self.PRESSURE_REGISTER + i)

        value = (bytes[2] << 16) | (bytes[1] << 8) | bytes[0]
        value = i2c.twos_complement(value, 3 * 8)
        return value / 4096.0

    def get_temperature(self):
        """
        Returns temperature in Celsius scale.
        """

        bytes = [0] * 2
        for i in range(2):
            bytes[i] = i2c.read_byte(self.BUS, self.ADDRESS, self.TEMPERATURE_REGISTER + i)

        value = (bytes[1] << 8) | bytes[0]
        value = i2c.twos_complement(value, 2 * 8)
        return 42.5 + value / 480.0