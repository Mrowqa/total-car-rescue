#!/usr/bin/python

import pigpio
import DHT22_PIGPIO
import time


class DHT22:
    """
    Humidity and temperature sensor
    """

    PI = pigpio.pi()
    DATA_VALIDITY_TIME_MILLISECONDS = 5

    def __init__(self, gpio_id=4):
        self.__sensor = DHT22_PIGPIO.sensor(self.PI, gpio_id)
        self.__last_read = 0
        self.__read_data_if_expired()
        time.sleep(2)  # time needed to initialize the sensor

    def get_humidity(self):
        """
        Returns humidity in percent.
        """
        self.__read_data_if_expired()
        return self.__sensor.humidity()

    def get_temperature(self):
        """
        Returns temperature in Celsius scale.
        """
        self.__read_data_if_expired()
        return self.__sensor.temperature()

    def __read_data_if_expired(self):
        time_since_last_read = time.time() - self.__last_read
        if time_since_last_read > self.DATA_VALIDITY_TIME_MILLISECONDS / 1000.0:
            self.__sensor.trigger()  # one read operation lasts 20 milliseconds
            self.__last_read = time.time()
