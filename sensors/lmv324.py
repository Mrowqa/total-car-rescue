#!/usr/bin/python

from mcp3424 import MCP3424
from math import log


class LMV324:
    """
    Microphone
    """

    def __init__(self, envelope_pin_id=0, adc_i2c_address=0x6A):
        self.__adc = MCP3424(adc_i2c_address)
        self.__envelope_pin_id = envelope_pin_id

    def get_sound_volume(self):
        """
        :return: sound volume in dB
        """
        raw_reading = self.__adc.read_analog(self.__envelope_pin_id)
        if raw_reading <= 0:
            raise IOError('Microphone is not connected! Check envelope pin in ADC, should be connected to pin id={}'
                          .format(self.__envelope_pin_id))
        volume = 10 * log(raw_reading * 1e4) + 25 ** raw_reading
        return volume
