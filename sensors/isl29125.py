#!/usr/bin/python

import i2c


class ISL29125:
    """
    Light sensor
    """
    BUS = i2c.get_bus()
    ADDRESS = 0x44  # 0b1000100

    OPERATING_MODE = dict(power_down=0b000, g=0b001, r=0b010, b=0b011,
                          standby=0b100, rgb=0b101, rg=0b110, gb=0b111)
    SENSING_RANGE = {375: 0, 10000: 1}  # key in lux
    RESOLUTION = {16: 0, 12: 1}  # key in bits

    CONFIGURATION_1_REGISTER = 0x01
    COLOR_REGISTERS = dict(r=0x0B, g=0x09, b=0x0D)

    def __init__(self, operating_mode='rgb', sensing_range=10000,
                 bit_resolution=16):
        self.__sensing_range = sensing_range
        self.__bit_resolution = bit_resolution
        settings = self.OPERATING_MODE[operating_mode] | \
            (self.SENSING_RANGE[sensing_range] << 3) | \
            (self.RESOLUTION[bit_resolution] << 4) | \
            (0 << 5)  # by default - no sync
        # device starts working after following write
        i2c.write_byte(self.BUS, self.ADDRESS, self.CONFIGURATION_1_REGISTER,
                       settings)

    def get_rgb_illuminance(self):
        """
        :return: illuminance in lux for each of the following colors red, green and blue as a list
        """
        result_rgb = [0] * 3
        color2index = dict(r=0, g=1, b=2)

        for color, address in self.COLOR_REGISTERS.items():
            l_byte = i2c.read_byte(self.BUS, self.ADDRESS, address)
            h_byte = i2c.read_byte(self.BUS, self.ADDRESS, address + 1)
            value = (h_byte << 8) | l_byte
            result_rgb[color2index[color]] = value

        scale_factor = 1.0 / (2 ** self.__bit_resolution - 1) * self.__sensing_range
        result_rgb = [value * scale_factor for value in result_rgb]

        return result_rgb
