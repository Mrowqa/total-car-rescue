#!/usr/bin/python

from tcr import ProbabilityEstimator, NetworkDataProvider
from probing import ProbingResults
from http_server import run_http_server
from time import sleep


def run():
    probing_results = ProbingResults()
    probability_estimator = ProbabilityEstimator()
    network_data_provider = NetworkDataProvider()

    # Disabled due to bug. Raw data is named the same like the processed one.
    # probing_results.add_data_observer(network_data_provider)
    probability_estimator.add_data_observer(network_data_provider)

    probability_estimator.start()
    network_data_provider.start_websocket_server('0.0.0.0', 2345)
    run_http_server('0.0.0.0', 8080)

    while True:
        sleep(1)

run()
