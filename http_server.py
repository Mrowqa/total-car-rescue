#!/usr/bin/python

from bottle import Bottle, static_file
from threading import Thread


def run_http_server(host, port):
    app = Bottle()

    @app.route('/<path:path>')
    def get_static_file(path):
        return static_file(path, root='public_html/')

    thread = Thread(target=app.run, kwargs=dict(host=host, port=port))
    thread.daemon = True
    thread.start()

