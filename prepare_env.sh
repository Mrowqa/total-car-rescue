#!/bin/sh
if [ "$1" == "-use_pwd" ]; then
    PROJECT_ROOT=$(pwd)
else
    PROJECT_ROOT=$(dirname $0)
fi
if [ "$PROJECT_ROOT" == "" ]; then
    echo Invalid project root. Consider using '-use_pwd' option.
    return 1
fi
EXTLIB_DIR=$PROJECT_ROOT/extlib

export I2C_API=$EXTLIB_DIR/i2c
export DHT22_PIGPIO=$EXTLIB_DIR/DHT22_PIGPIO_lib

export PYTHONPATH=$PYTHONPATH:$PROJECT_ROOT:$I2C_API:$DHT22_PIGPIO
