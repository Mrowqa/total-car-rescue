#!/bin/bash
if [ $(id -u) -ne 0 ]; then
    echo Only root can run this script\!
    exit 0
fi

# Start pigpiod daemon
pigpiod

# Set time from the external RTC
hwclock -s

# Init ADC
echo mcp3424 0x6A > /sys/bus/i2c/devices/i2c-1/new_device
