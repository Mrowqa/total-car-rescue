#!/usr/bin/python

# Based on bitify's wrapper

from smbus import SMBus
from os.path import exists

def raspberry_pi_bus_number():
    """Returns Raspberry Pi I2C bus number (integer, 0 or 1) if available,
    None otherwise.

    Checks which `/dev/i2c-0` or `/dev/i2c-1` is available and returns
    appropriate number.
    """
    if exists('/dev/i2c-0'):
        return 0
    if exists('/dev/i2c-1'):
        return 1
    return None

def get_bus():
	return SMBus(raspberry_pi_bus_number())
    
def read_byte(bus, address, register):
    return bus.read_byte_data(address, register)
 
def read_word_unsigned(bus, address, register):
    high = bus.read_byte_data(address, register)
    low = bus.read_byte_data(address, register+1)
    return (high << 8) + low

def read_word_signed(bus, address, register):
    value = read_word_unsigned(bus, address, register)
    if (value >= 0x8000):
        return -((0xffff - value) + 1)
    else:
        return value

def write_byte(bus, address, register, value):
    bus.write_byte_data(address, register, value)

def read_block(bus, address, start, length):
    return bus.read_i2c_block_data(address, start, length)

def twos_complement(value, bits_cnt):
    if (value >= (1 << ((bits_cnt-1)))):
        return -(((1<<bits_cnt) - 1 - value) + 1)
    else:
        return value

def reverse_bits(value, bits_cnt):  # input must be unsigned!
	reversed_value = bin(value)[2:][::1]
	return int((reversed_value+'0'*bits_cnt)[:bits_cnt],2)
        
if __name__ == "__main__":
    print(raspberry_pi_bus_number())
