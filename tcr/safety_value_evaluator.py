#!/usr/bin/python

"""
This module is responsible for evaluating Safety Values for parameters and groups. It provides two methods, one for
parameters and another for groups. This module also contains definitions of groups.
"""

from numpy import mean


# TODO: check this method! Wojtek made some mistakes and it is possible that I (Artur) haven't found all of them
def calculate_safety_value_for_parameter(parameter_name, arg):
    if parameter_name == 'current time':
        minutes_from_2pm = arg
        dividers = [(7 * 60, 17.5), (8 * 60 + 59, 15), (9 * 60, 15.4), (10 * 60, 16.4), (11 * 60, 17.4),
                    (12 * 60, 18.4), (14 * 60, 18.1), (14 * 60 + 15, 19.1), (15 * 60 + 15, 27), (16 * 60 + 15, 54),
                    (17 * 60 + 15, 85), (18 * 60 + 15, 180), (19 * 60 + 15, 570), (23 * 60 + 59, 1502)]
        for upper_bound, divider_value in dividers:
            if minutes_from_2pm < upper_bound:
                return minutes_from_2pm / divider_value

    if parameter_name == 'illuminance':
        E = mean(arg)
        if E < 25:
            return (3511 - 65 * E) / 22.0
        if E < 50:
            return 107 - E
        if E <= 100:
            return 78 - (21 * E) / 50.0
        if E < 250:
            return (2.25e5 / E ** 2) - (E / 210.0) ** 3 - (E / 50.0) + 15
        if E < 500:
            return (2000 / E) - (E / 500.0) + 2.5
        if E <= 800:
            return 2000 / E
        if E > 800:
            return 1 + (100 / E ** 2)

    if parameter_name == 'time since last significant speed change':
        T = arg
        if T == 0:  # FIXME: what documentation says about this? There's a division by 0 below!
            T += 0.1
        if T < 50:
            return (T / 2.5) - (4.0 / T)
        if T <= 120:
            return (4 * T / 7.0) - 10
        if T < 200:
            return (T ** 2) / 250.0
        if T >= 200:
            return (T ** 3) / 50000.0

    if parameter_name == 'atmospheric pressure':
        p = arg
        if p <= 960:
            return (p / 900.0) ** 18
        if p <= 990:
            return 150 / (1000 - p)
        if p < 1020:
            return (1020 - p) / 2.0 + 2.5
        if p >= 1050:
            return (1051 - p) / 7.0

    if parameter_name == 'air temperature':
        T = arg
        if T < 17:
            return T ** 2 / 45.0
        if T <= 24:
            return (T / 7.0) ** 3 + (T / 4) ** 2 - 25
        if T < 31:
            return 127 - (T ** 2 / 10.0) - (T / 10.0) ** 3
        if T <= 41:
            return 200 / T + T ** 3 / 20000.0 + 2.5

    if parameter_name == 'air humidity':
        H = arg
        if H <= 40:
            return H / 5.0 + 2
        if H <= 70:
            return 2 * H / 9.0 - 2 * H ** 2 / 5000.0 + 1.4
        if H <= 90:
            return 8.2e4 / H ** 2
        if H <= 100:
            return (H - 90) / 5.0 - H + 101.75

    if parameter_name == 'travel time':
        TT = arg
        if TT <= 60:
            return 1 / 1.4 * (TT / 3.0 + 2)
        if TT <= 120:
            return 1 / 1.4 * (TT / 15.0) * 2
        if TT < 240:
            return 1.5 / 1.4 * (TT / 45.0) ** 3 + 36
        if TT >= 240:
            return 1 / 1.4 * (TT / 2.0 + TT ** 2 / 1000.0 + 110)

    if parameter_name == 'pulse':
        HR = arg
        if HR <= 70:
            return 1 / 1.8 * (HR ** 4) / 30000.0 - HR ** 2 / 2.01 + 25 * HR
        if HR <= 90:
            return 262 - (HR ** 2 / 25.0) + 4 * (HR % 71)
        if HR <= 150:
            return 6e10 / HR ** 5 + 2

    if parameter_name == 'sound volume':
        I = arg
        if I == 0:  # FIXME: what documentation says about this? There's a division by 0 below!
            I += 0.1
        if I < 40:
            return 200.0 / I ** 0.5 - I + 51
        if I < 60:
            return (I ** 5 / 3.5e7) + 1
        if I <= 125:
            return 3e7 / I ** 4 + 3.5e4 / I ** 2 - 2

    if parameter_name == 'time since last turning':
        T = arg
        if T < 26:
            return T ** 2 / 5.0 + 3 * T % 84 - T / 2.0
        if T <= 120:
            return (1500 * T) ** 0.5 + T / 2.0
        if T > 120:
            return 4 * T

    raise NameError('Non-existing parameter: "{}" or incorrect parameter argument: "{}"!'.format(parameter_name, arg))

# FORMAT: group_name: tuple of pairs, each pair contain a parameter name and its contribution factor
GROUP_DEFINITIONS = {
    'group 1': (('time since last turning', 0.7), ('time since last significant speed change', 0.3)),
    'group 2': (('air temperature', 0.5), ('atmospheric pressure', 0.25), ('air humidity', 0.25)),
    'group 3': (('pulse', 1),),
    'group 4': (('travel time', 0.45), ('current time', 0.3), ('illuminance', 0.25)),
    'group 5': (('sound volume', 1),)
}


def calculate_safety_value_for_group(group_name, current_data_samples):
    group_definition = GROUP_DEFINITIONS[group_name]
    group_safety_value = 0
    for parameter_name, parameter_contribution_factor in group_definition:
        parameter_data_sample = current_data_samples[parameter_name]
        parameter_safety_value = calculate_safety_value_for_parameter(parameter_name, parameter_data_sample)
        group_safety_value += parameter_safety_value * parameter_contribution_factor
    return group_safety_value
