#!/usr/bin/python

import SocketServer
from threading import Thread, Lock
from probing.parameters_probing_config import PARAMETERS_PROBES_CONFIG
from tcr.probability_estimator import ProbabilityEstimator
from tcr.safety_value_evaluator import GROUP_DEFINITIONS
from utils.singleton import Singleton
from time import sleep, time
import json
from bottle import Bottle
from bottle.ext.websocket import GeventWebSocketServer
from bottle.ext.websocket import websocket


# TODO: refactor this code for multiple data providers. Current code was written ASAP due to coming deadline.

class NetworkDataProvider:
    """
    This class is a singleton. It allows to share data such as parameters data samples or Safety Values via TCP sockets.
    To start server, class start method with host and port as a arguments, for example:
    NetworkDataProvider().start('localhost', 2345)
    """
    __metaclass__ = Singleton

    DATA_PROVIDING_FREQUENCY = 100

    def __init__(self):
        self.__clients = list()
        self.__clients_lock = Lock()
        self.__data_to_provide = dict()
        self.__data_lock = Lock()

        self.__tcp_server = None
        self.__tcp_server_thread = None
        self.__websocket_server = None
        self.__websocket_server_server_thread = None

        self.__data_providing_thread = Thread(target=self.__start_providing_data)
        self.__data_providing_thread.daemon = True
        self.__data_providing_thread.start()

    def start_tcp_server(self, host, port):
        """
        This method could be called only once (remember this class is a singleton!).
        """
        self.__tcp_server = SocketServer.TCPServer((host, port), ConnectionHandler)
        self.__tcp_server_thread = Thread(target=self.__tcp_server.serve_forever)
        self.__tcp_server_thread.daemon = True
        self.__tcp_server_thread.start()

    def start_websocket_server(self, host, port):
        """
        This method could be called only once (remember this class is a singleton!).
        """
        self.__websocket_server = WebSocketConnectionHandler.SERVER_APP
        server_run_arguments = dict(host=host, port=port, server=GeventWebSocketServer)
        self.__websocket_server_server_thread = Thread(target=self.__websocket_server.run, kwargs=server_run_arguments)
        self.__websocket_server_server_thread.daemon = True
        self.__websocket_server_server_thread.start()

    def on_parameters_data_samples_get(self, data_samples):
        self.__data_lock.acquire()
        for name, values_list in data_samples.iteritems():
            if name not in self.__data_to_provide:
                self.__data_to_provide[name] = list()
            self.__data_to_provide[name].extend(values_list)
        self.__data_lock.release()

    def on_safety_values_get(self, safety_values):
        # this method does almost the same as the one above
        data_samples = {name: list([value]) for name, value in safety_values.iteritems()}
        self.on_parameters_data_samples_get(data_samples)

    def register_client(self, client):
        self.__clients_lock.acquire()
        self.__clients.append(client)
        self.__clients_lock.release()

    def unregister_client(self, client):
        self.__clients_lock.acquire()
        if client in self.__clients:
            self.__clients.remove(client)
        self.__clients_lock.release()

    def __start_providing_data(self):
        while True:
            start_time = time()
            self.__provide_data()
            sleep_time = 1.0 / self.DATA_PROVIDING_FREQUENCY - (time() - start_time)
            if sleep_time > 0:
                sleep(sleep_time)

    def __provide_data(self):
        self.__data_lock.acquire()
        data_to_provide = self.__data_to_provide
        self.__data_to_provide = dict()
        self.__data_lock.release()
        if not data_to_provide:
            return

        self.__clients_lock.acquire()
        for client in self.__clients:
            client.on_data_get(data_to_provide)
        self.__clients_lock.release()


class ConnectionHandler:
    __AVAILABLE_PARAMETERS_NAMES = PARAMETERS_PROBES_CONFIG.keys() + GROUP_DEFINITIONS.keys()

    class Disconnected(Exception):
        pass

    def init(self):
        """
        This class doesn't have __init__ method due to problems with multiple inheritance.
        """
        self.__data_to_send = dict()
        self.__data_lock = Lock()
        self.__chosen_parameters = list()
        self.setup()

    def on_data_get(self, new_data):
        """
        This method is called by NetworkDataProvider class in order to provide new data samples to this client.
        """
        self.__data_lock.acquire()
        for name in self.__chosen_parameters:
            if name in new_data:
                if name not in self.__data_to_send:
                    self.__data_to_send[name] = list()
                self.__data_to_send[name].extend(new_data[name])
        self.__data_lock.release()

    def start_working(self):
        self.init()
        try:
            self.handle()
        except self.Disconnected:
            pass
        self.finish()

    def setup(self):
        """
        This method is called before handle() and is used to prepare connection. You should override this method, if you
        want to execute code before handling with the connection. The default implementation does nothing.
        """
        pass

    def handle(self):
        """
        handle() is main method for handling with the connection and mustn't be overloaded!
        """
        self.__check_request_validity()
        NetworkDataProvider().register_client(self)
        self.__send_welcoming_response()
        self.__serve()

    def __check_request_validity(self):
        json_request = self.receive_packet()
        try:
            self.__chosen_parameters = json.loads(json_request)
        except ValueError:
            self.__fail('Incorrect JSON object!')

        if type(self.__chosen_parameters) is not list:
            self.__fail('You have to provide JSON list with wanted parameter names!')

        for parameter_name in self.__chosen_parameters:
            if type(parameter_name) is not str and type(parameter_name) is not unicode:
                self.__fail('You have to provide JSON list with wanted parameter names!')
            if parameter_name not in self.__AVAILABLE_PARAMETERS_NAMES:
                self.__fail('"{}" parameter is not supported!'.format(parameter_name))
            self.__data_to_send[parameter_name] = list()

    def __fail(self, error_message):
        error_object = dict(error=error_message)
        self.send_packet(json.dumps(error_object))
        self.__close_connection()

    def __close_connection(self):
        self.close_connection()
        raise self.Disconnected('Connection closed.')

    def __send_welcoming_response(self):
        response = dict()
        for name in self.__chosen_parameters:
            if name in PARAMETERS_PROBES_CONFIG:
                response[name] = dict(type='parameter',
                                      unit=PARAMETERS_PROBES_CONFIG[name]['unit'],
                                      probing_frequency=PARAMETERS_PROBES_CONFIG[name]['probing_frequency'])
            else:
                response[name] = dict(type='group',
                                      estimating_frequency=ProbabilityEstimator().get_estimating_frequency())
        self.send_packet(json.dumps(response))

    def __serve(self):
        while True:
            request_content = self.receive_packet()
            if request_content != "GET":
                self.wfile.write('Incorrect request!\n')
                continue

            self.__data_lock.acquire()
            data_to_send = self.__data_to_send
            self.__data_to_send = dict()
            self.__data_lock.release()
            self.send_packet(json.dumps(data_to_send))

    def finish(self):
        """
        This method mustn't be overloaded!
        """
        NetworkDataProvider().unregister_client(self)

    def send_packet(self, packet):
        raise NotImplementedError("ConnectionHandler is an abstract class! Please inherit it and define this method.")

    def receive_packet(self):
        raise NotImplementedError("ConnectionHandler is an abstract class! Please inherit it and define this method.")

    def close_connection(self):
        raise NotImplementedError("ConnectionHandler is an abstract class! Please inherit it and define this method.")


class TCPConnectionHandler(ConnectionHandler, SocketServer.StreamRequestHandler):

    def send_packet(self, packet):
        self.wfile.write(packet + '\n')

    def receive_packet(self):
        return self.rfile.readline()

    def close_connection(self):
        self.request.close()


class WebSocketConnectionHandler(ConnectionHandler):

    SERVER_APP = Bottle()

    @staticmethod
    @SERVER_APP.get('/data_stream', apply=[websocket])
    def init_new_connection(websocket_handle):
        connection_handler = WebSocketConnectionHandler()
        connection_handler.websocket = websocket_handle
        connection_handler.start_working()

    def send_packet(self, packet):
        self.websocket.send(packet)

    def receive_packet(self):
        message = self.websocket.receive()
        if message is None:
            raise self.Disconnected('Connection closed.')
        return message

    def close_connection(self):
        self.websocket.close()