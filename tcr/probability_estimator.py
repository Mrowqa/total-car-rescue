#!/usr/bin/python

from probing import ProbingResults, MAJOR_PARAMETERS_NAMES
from safety_value_evaluator import calculate_safety_value_for_parameter, calculate_safety_value_for_group, \
    GROUP_DEFINITIONS
from utils.singleton import Singleton
from threading import Thread
from time import time, sleep


class ProbabilityEstimator:
    """
    Main class of Total Car Rescue project. In order to use this class, instantiate object of this class and set alarm
    callback by using set_alarm_callback() method (default callback does nothing). If you want to receive current safety
    values and parameters data samples, use add_data_observer() method. When you prepare system to work, call start()
    method.
    """
    __metaclass__ = Singleton

    def __init__(self, estimating_frequency=30):
        self.__estimating_frequency = estimating_frequency
        self.__probing_results = ProbingResults()
        self.__alarm_callback = lambda: None
        self.__data_observers = list()
        self.__current_safety_values = dict()
        self.__thread = Thread(target=self.__estimator_thread)
        self.__thread.daemon = True

    def set_alarm_callback(self, alarm_callback):
        self.__alarm_callback = alarm_callback

    def __check_if_is_alarm(self):
        # TODO: make a research and define this method!
        return False

    def start(self):
        self.__probing_results.start()
        self.__thread.start()

    def add_data_observer(self, observer):
        """
        Observer is notified with new safety values when they are evaluated. If you want subscribe to parameters data
        sample, use ProbingResults class (it's a singleton).
        :param observer: object with on_safety_values_get(arg) method. Argument for this method is a dict containing
        safety values.
        """
        self.__data_observers.append(observer)

    def remove_data_observer(self, observer):
        self.__data_observers.remove(observer)

    def get_estimating_frequency(self):
        return self.__estimating_frequency

    def __estimator_thread(self):
        while True:
            measurement_start_time = time()
            self.__estimator_loop()
            sleep_time = 1.0 / self.__estimating_frequency - (time() - measurement_start_time)
            if sleep_time > 0:
                sleep(sleep_time)

    def __estimator_loop(self):
        self.__evaluate_safety_values()
        if self.__check_if_is_alarm():
            self.__alarm_callback()
        self.__provide_safety_values_to_observers()

    def __evaluate_safety_values(self):
        current_parameters_data_samples = \
            self.__probing_results.get_current_values_of_chosen_parameters(MAJOR_PARAMETERS_NAMES)

        for parameter_name in MAJOR_PARAMETERS_NAMES:
            self.__current_safety_values[parameter_name] = \
                calculate_safety_value_for_parameter(parameter_name, current_parameters_data_samples[parameter_name])
        for group_name in GROUP_DEFINITIONS.iterkeys():
            self.__current_safety_values[group_name] = \
                calculate_safety_value_for_group(group_name, current_parameters_data_samples)

    def __provide_safety_values_to_observers(self):
        for observer in self.__data_observers:
            observer.on_safety_values_get(self.__current_safety_values)
