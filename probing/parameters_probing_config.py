#!/usr/bin/python

"""
Parameters config is located near the end of file. It is a list of tuples (converted into dict of dicts) with
the following information:
* name of a parameter - used as key in config dict,
* unit/format - additional information,
* frequency of probing - used in ParameterProbe for telling how frequent probe new data sample
* probe data sample method - this method is called to obtain new data sample
All above this configuration is preparing methods for probing data samples.
"""

from sensors import *
from time import time
from math import sqrt
from uptime import uptime
from datetime import datetime
from threading import Lock


# Defining sensors
heart_monitor = AD8232(1, 0x6A)
dht = DHT22(4)
light_sensor = ISL29125('rgb', 10000, 16)
gyroscope = L3GD20H('xyz')
gyroscope.set_calibration_offset(-1.75, +9, -1.5)
microphone = LMV324(0, 0x6A)
barometer = LPS331AP('active', (7, False))
accelerometer = LSM303D(100, 3.125, '+-4 g', '+-2 gauss')  # TODO: add possibility to disable magnetometer


# Helper function
vector_magnitude = lambda x: sqrt(sum(i ** 2 for i in x))


# Defining helper classes for better performance during probing
class GyroscopeParametersMeasurementHelper:
    TURNING_ANGULAR_SPEED_DETECTION_BOUND = 5

    def __init__(self):
        self.__last_detection = time()
        self.__last_measurement = 0
        self.__last_measurement_lock = Lock()

    def get_time_since_last_turning(self):
        self.__last_measurement_lock.acquire()
        last_measurement = self.__last_measurement = gyroscope.get_xyz_angular_velocity()
        self.__last_measurement_lock.release()
        if vector_magnitude(last_measurement) >= self.TURNING_ANGULAR_SPEED_DETECTION_BOUND:
            self.__last_detection = time()
            return 0
        return time() - self.__last_detection

    def get_last_measurement(self):
        self.__last_measurement_lock.acquire()
        last_measurement = self.__last_measurement
        self.__last_measurement_lock.release()
        return last_measurement


class AccelerometerParametersMeasurementHelper:
    def __init__(self):
        self.__last_detection = time()
        self.__last_measurement = 0
        self.__last_measurement_lock = Lock()

    def get_time_since_last_significant_speed_change(self):
        # TODO: implement detection according to documentation!
        # (model is static, so this functionality isn't necessary at this moment)
        self.__last_measurement_lock.acquire()
        self.__last_measurement = accelerometer.get_acceleration()
        self.__last_measurement_lock.release()
        return time() - self.__last_detection

    def get_last_measurement(self):
        self.__last_measurement_lock.acquire()
        last_measurement = self.__last_measurement
        self.__last_measurement_lock.release()
        return last_measurement


class HeartMonitorHelper:
    def __init__(self, probing_frequency):
        self.__historic_data = list()
        self.__last_measurement = 90
        self.__last_measurement_lock = Lock()
        self.__HISTORY_LENGTH_IN_SECONDS = 5
        self.__probing_frequency = probing_frequency
        self.__MAX_DATA_SAMPLES_COUNT = int(self.__HISTORY_LENGTH_IN_SECONDS * self.__probing_frequency) + 1

    def get_pulse(self):
        self.__last_measurement_lock.acquire()
        last_measurement = self.__last_measurement = heart_monitor.get_current_ecg_value()
        self.__last_measurement_lock.acquire()
        self.__historic_data.insert(0, last_measurement)
        is_there_enough_data_samples = len(self.__historic_data) >= self.__MAX_DATA_SAMPLES_COUNT
        if not is_there_enough_data_samples:
            return 90

        # TODO: optimize this algorithm (use cache)
        # TODO: make this algorithm more accurate: measure time between beats
        upper_signal_bound = 0.8
        count_of_samples_above_bound_in_row = 0
        samples_count_above_bound_per_beat = 5  # do it dependent on frequency?
        beats = 0
        for data_sample in self.__historic_data:
            if data_sample < upper_signal_bound:
                count_of_samples_above_bound_in_row = 0
                continue
            count_of_samples_above_bound_in_row += 1
            if count_of_samples_above_bound_in_row == samples_count_above_bound_per_beat:
                beats += 1
        return beats * 60 / self.__HISTORY_LENGTH_IN_SECONDS

    def get_last_measurement(self):
        self.__last_measurement_lock.acquire()
        last_measurement = self.__last_measurement
        self.__last_measurement_lock.release()
        return last_measurement


def get_current_time():
    """
    :return: number of minutes since 2 PM
    """
    now = datetime.now()
    return ((now.hour - 14) % 24) * 60 + now.minute + now.second / 60.0

# Defining objects of helper classes
gyroscope_parameters_measurement_helper = GyroscopeParametersMeasurementHelper()
accelerometer_parameters_measurement_helper = AccelerometerParametersMeasurementHelper()
heart_monitor_probing_frequency = 200
heart_monitor_helper = HeartMonitorHelper(heart_monitor_probing_frequency)


# FORMAT: (name, unit/format, probing_frequency, probe_data_sample, is_major, neutral_value)
# This list is kept due to its high readability and converted into dict below to easier usage.
_PARAMETERS_PROBES_CONFIG_LIST = (
    ("angular speed", "d/s", 10, gyroscope_parameters_measurement_helper.get_last_measurement, False, (0, 0, 0)),
    ("time since last turning", "s", 60, gyroscope_parameters_measurement_helper.get_time_since_last_turning, True, 0),
    ("acceleration", "m/s**2", 10, accelerometer_parameters_measurement_helper.get_last_measurement, False, (0, 0, 0)),
    ("time since last significant speed change", "s", 60,
     accelerometer_parameters_measurement_helper.get_time_since_last_significant_speed_change, True, 0),
    ("atmospheric pressure", "hPa", 5, barometer.get_pressure, True, 1000),
    ("illuminance", "lx", 10, light_sensor.get_rgb_illuminance, True, 300),
    ("sound volume", "dB", 10, microphone.get_sound_volume, True, 30),
    ("air temperature", "*C", 1, dht.get_temperature, True, 20),
    ("air humidity", "% RH", 1, dht.get_humidity, True, 40),
    ("ECG", "-", heart_monitor_probing_frequency, heart_monitor_helper.get_last_measurement, False, 0.5),
    ("pulse", "BPM", heart_monitor_probing_frequency, heart_monitor_helper.get_pulse, True, 90),
    ("travel time", "min", 1, lambda: uptime() / 60.0, True, 0),
    ("current time", "min", 1, get_current_time, True, 0)
)


# Build a dictionary based on list above to easier referring to data
PARAMETERS_PROBES_CONFIG = dict()
for entry in _PARAMETERS_PROBES_CONFIG_LIST:
    PARAMETERS_PROBES_CONFIG[entry[0]] = dict(name=entry[0], unit=entry[1], probing_frequency=entry[2],
                                              probe_data_sample=entry[3], is_major=entry[4], neutral_value=entry[5])


MAJOR_PARAMETERS_NAMES = tuple(
    parameter['name'] for parameter in PARAMETERS_PROBES_CONFIG.itervalues() if parameter['is_major']
)
