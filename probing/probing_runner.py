#!/usr/bin/python


class ProbingRunner:
    """
    Helper class responsible for starting and stopping probing parameters. You have to provide list of ParameterProbe
    instances, which you want to be managed, to the constructor.
    """

    def __init__(self, parameter_probes):
        self.__parameter_probes = parameter_probes

    def start_probing(self):
        for parameter_probe in self.__parameter_probes:
            parameter_probe.start_probing()

    def stop_probing(self):
        for parameter_probe in self.__parameter_probes:
            parameter_probe.stop_probing()

    def is_probing(self):
        for parameter_probe in self.__parameter_probes:
            if parameter_probe.is_probing():
                return True
        return False
