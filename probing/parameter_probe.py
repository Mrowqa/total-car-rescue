#!/usr/bin/python

from threading import Thread, Lock
from time import sleep, time
from probing.parameters_probing_config import get_current_time


class ParameterProbe:
    """
    Class responsible for continuously probing given parameter in separate thread.

    To use this class you must provide parameter name, probing frequency and method which called without any arguments
    returns new probed data sample. If you want start the probing process, you should call start_probing() method. Then
    you can read pending data by calling get_pending_data() method.
    """

    MAX_KEPT_HISTORY_IN_SECONDS = 5.0

    def __init__(self, name, probing_frequency, probe_method):
        """
        :param name: name of parameter which is destined to be probed
        :param probing_frequency: frequency of probing (calling probe_method) in hertz
        :param probe_method: this method called without any parameters should return new probed data sample
        """
        self.__name = name
        self.__probing_frequency = probing_frequency
        self.__MAX_DATA_SAMPLES_COUNT = int(self.MAX_KEPT_HISTORY_IN_SECONDS * probing_frequency) + 1
        self.__probe_data_sample = probe_method
        self.__historic_data = list()
        self.__data_lock = Lock()
        self.__is_probing = False
        self.__thread = Thread(target=self.__probing_thread)
        self.__thread.daemon = True
        self.__thread.start()

    def get_name(self):
        return self.__name

    def get_probing_frequency(self):
        return self.__probing_frequency

    def get_pending_data(self):
        self.__data_lock.acquire()
        pending_data = self.__historic_data
        self.__historic_data = list()
        self.__data_lock.release()
        return pending_data

    def start_probing(self):
        self.__is_probing = True

    def stop_probing(self):
        self.__is_probing = False
        self.__clear_history()

    def is_probing(self):
        return self.__is_probing

    def __clear_history(self):
        self.__data_lock.acquire()
        self.__historic_data.clear()
        self.__data_lock.release()

    def __probing_thread(self):
        while True:
            start_time = time()
            if self.__is_probing:
                self.__probe()
            sleep_time = 1.0 / self.__probing_frequency - (time() - start_time)
            if sleep_time > 0:
                sleep(sleep_time)

    def __probe(self):
        new_value = self.__probe_data_sample()
        self.__data_lock.acquire()
        self.__historic_data.insert(0, new_value)
        if len(self.__historic_data) > self.__MAX_DATA_SAMPLES_COUNT:
            self.__historic_data.pop()
        self.__data_lock.release()
