#!/usr/bin/python

from parameters_probing_config import PARAMETERS_PROBES_CONFIG
from parameter_probe import ParameterProbe
from probing_runner import ProbingRunner
from utils.singleton import Singleton
from threading import Thread, Lock
from time import sleep


class ProbingResults:
    """
    Class responsible for getting data samples of probed parameters. To start probing, call start() method.

    You can get current parameters data samples by calling get_current_values_of_all_parameters() or
    get_current_values_of_chosen_parameters() methods. You can also get historical samples by using
    get_historic_values_of_all_parameters() or get_historic_values_of_chosen_parameters() methods.

    If you want to be notified with new data samples, call add_data_observer() before calling start() method.
    """
    __metaclass__ = Singleton

    MAX_KEPT_HISTORY_IN_SECONDS = 60

    def __init__(self):
        self.PARAMETERS_PROBES_CONFIG = PARAMETERS_PROBES_CONFIG
        self.__parameter_probes = self.__construct_parameter_probes_list_from_config(self.PARAMETERS_PROBES_CONFIG)
        self.__probing_runner = ProbingRunner(self.__parameter_probes)
        self.__data_observers = list()
        self.__historic_data = {name: list() for name in self.PARAMETERS_PROBES_CONFIG.iterkeys()}
        self.__MAX_DATA_SAMPLES_COUNT = {
            parameter['name']: int(parameter['probing_frequency'] * self.MAX_KEPT_HISTORY_IN_SECONDS) + 1
            for parameter in self.PARAMETERS_PROBES_CONFIG.itervalues()
        }
        self.__data_lock = Lock()
        self.__data_updater = Thread(target=self.__data_samples_updater)
        self.__data_updater.daemon = True

    @staticmethod
    def __construct_parameter_probes_list_from_config(config):
        parameter_probes = list()
        for _, probe_config in config.iteritems():
            name = probe_config['name']
            probing_frequency = probe_config['probing_frequency']
            probe_data_sample = probe_config['probe_data_sample']
            new_parameter_probe = ParameterProbe(name, probing_frequency, probe_data_sample)
            parameter_probes.append(new_parameter_probe)
        return parameter_probes

    def start(self):
        self.__probing_runner.start_probing()
        self.__data_updater.start()

    def add_data_observer(self, observer):
        """
        Observer is notified with new parameters data samples when they are available.
        :param observer: object with on_parameters_data_samples_get(arg) method. Argument of this method is a dict
        containing parameters data samples.
        """
        self.__data_observers.append(observer)

    def remove_data_observer(self, observer):
        self.__data_observers.remove(observer)

    def get_current_values_of_all_parameters(self):
        return self.get_current_values_of_chosen_parameters(self.__historic_data.keys())

    def get_current_values_of_chosen_parameters(self, chosen):
        self.__update_data_samples()
        result = dict()
        self.__data_lock.acquire()
        for parameter_name in chosen:
            data_samples = self.__historic_data[parameter_name]
            result[parameter_name] = data_samples[0] if data_samples \
                else PARAMETERS_PROBES_CONFIG[parameter_name]['neutral_value']
        self.__data_lock.release()
        return result

    def get_historic_values_of_all_parameters(self, history_length_in_seconds=MAX_KEPT_HISTORY_IN_SECONDS):
        return self.get_historic_values_of_chosen_parameters(self.__historic_data.keys(), history_length_in_seconds)

    def get_historic_values_of_chosen_parameters(self, chosen, history_length_in_seconds=MAX_KEPT_HISTORY_IN_SECONDS):
        self.__update_data_samples()
        result = dict()
        history_length_in_seconds = min(history_length_in_seconds, self.MAX_KEPT_HISTORY_IN_SECONDS)
        history_length_in_seconds = max(history_length_in_seconds, 0)
        self.__data_lock.acquire()
        for parameter_name in chosen:
            data_samples = self.__historic_data[parameter_name]
            requested_samples_count = int(history_length_in_seconds *
                                          self.PARAMETERS_PROBES_CONFIG[parameter_name]['probing_frequency'])
            result[parameter_name] = data_samples[0:requested_samples_count]
        self.__data_lock.release()
        return result

    def __update_data_samples(self):
        if not self.__probing_runner.is_probing:
            self.clear_historic_data()
            return

        self.__new_data_samples = dict()
        for probe in self.__parameter_probes:
            self.__new_data_samples[probe.get_name()] = probe.get_pending_data()
        self.__data_lock.acquire()
        for name, data_samples in self.__new_data_samples.iteritems():
            self.__historic_data[name][0:0] = data_samples
            samples_limit = self.__MAX_DATA_SAMPLES_COUNT[name]
            if len(self.__historic_data[name]) > samples_limit:
                del self.__historic_data[name][samples_limit:]
        self.__data_lock.release()

    def clear_historic_data(self):
        self.__data_lock.acquire()
        for parameter_name in self.__historic_data.iterkeys():
            self.__historic_data[parameter_name].clear()
        self.__data_lock.release()

    def __data_samples_updater(self):
        """
        This method is destined for daemon which takes care of getting data sample from parameter probes before these
        samples are deleted.
        """
        while True:
            sleep(ParameterProbe.MAX_KEPT_HISTORY_IN_SECONDS * 0.8)
            self.__update_data_samples()
            self.__provide_new_data_samples_to_observers()

    def __provide_new_data_samples_to_observers(self):
        for observer in self.__data_observers:
            observer.on_parameters_data_samples_get(self.__new_data_samples)
