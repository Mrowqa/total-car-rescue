var dataSource = "ws://{address}:2345/data_stream".format({address: location.hostname});
var allDataNames = [
	"time since last turning",
	"time since last significant speed change",
	"atmospheric pressure",
	"illuminance",
	"sound volume",
	"air temperature",
	"air humidity",
	"pulse",
	"travel time",
	"current time",
	
	"group 1",
	"group 2",
	"group 3",
	"group 4",
	"group 5"
];

var communicationAccepted = false;
var dataInfo = null;
var dataGetFrequency = 5; // Hz
var dataDisplayDelay = 0.2; // sec

var chartsHandles = {}
var dataType2Id = {parameter: "parameters", group: "groups"};

function startStreamingData() {
    websocket = new WebSocket(dataSource);
	websocket.addEventListener('open', onOpen);
    websocket.addEventListener('message', onMessage);
    websocket.addEventListener('close', onEnd);
    // websocket.addEventListener('error', onEnd);
}

function onOpen(evt) {
	websocket.send(JSON.stringify(allDataNames));
}

function onMessage(evt) {
	if(communicationAccepted == false)
		initCommunication(evt);
	else
		processData(evt);
}

function onEnd(evt) {
	alert(('Connection closed or error occured: "{message}".\n' +
		   'Please refresh page to connect again.').format({message: evt.data}));
}

function initCommunication(evt) {
	var response = JSON.parse(evt.data);
	if("error" in response) {
		console.log(response["error"]);
		return;
	}
	communicationAccepted = true;
	dataInfo = response;
	initCharts();
	setInterval(function() {
		websocket.send('GET');
	}, 1 / dataGetFrequency * 1000);
}

function processData(evt) {
	responseData = JSON.parse(evt.data);
	for(name in responseData) {
		chartsHandles[name].series[0].append(
			new Date().getTime(),
			responseData[name][responseData[name].length-1]
			// all data should be added according to the estimating frequency!
		);
	}
}

function initCharts() {
	for(var name in dataInfo) {
		var container = document.createElement("div");
		container.className = "col-lg-6";
		
		var dataType = dataInfo[name].type;
		var rowOfChartsId = dataType2Id[dataType] + "_charts";
		document.getElementById(rowOfChartsId).appendChild(container);
		
		
		var header = document.createElement("h1");
		header.textContent = name;
		container.appendChild(header);
		
		var chartElement = document.createElement("canvas");
		chartElement.width = 500;
		chartElement.height = 300;
		container.appendChild(chartElement);
		
		
		var smoothie = new SmoothieChart();
		smoothie.streamTo(chartElement, dataDisplayDelay * 1000);
		var line = new TimeSeries();
		smoothie.addTimeSeries(line);
		
		chartsHandles[name] = {chart: smoothie, series: [line]};
	}
}


startStreamingData();
