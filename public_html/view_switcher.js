
allIds = ["raw_data", "parameters", "groups"];
	
function switchView(viewId) {
	if(allIds.indexOf(viewId) == -1)
		return;
	for(i=0; i<allIds.length; i++)
		hideView(allIds[i]);
	showView(viewId);
}

function hideView(viewId) {
	document.getElementById(viewId + "_tab").style.display = "none";
	document.getElementById(viewId + "_nav").className = "";
}

function showView(viewId) {
	document.getElementById(viewId + "_tab").style.display = "block";
	document.getElementById(viewId + "_nav").className = "active";
}
